export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: 'Reqcheq Test',
    meta: [
      {charset: 'utf-8' },
      {name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {hid: 'og-title', property: 'og:title', content: 'Regcheq'}
    ],
    link: [
      // { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  vue: {
    config: {
      productionTip: false,
      devtools: true
    }
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/axios-accessor'
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    // '@nuxtjs/eslint-module'
    '@nuxtjs/vuetify',
    '@nuxt/typescript-build',
    'nuxt-typed-vuex',
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    '@nuxtjs/i18n',
    ['nuxt-fontawesome', {
      imports: [
        {
          set: '@fortawesome/free-solid-svg-icons',
          icons: ['fas']
        },
        {
          set: '@fortawesome/free-brands-svg-icons',
          icons: ['fab']
        }
      ]
    }]
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
    baseURL: 'https://api.escuelajs.co/api/v1/'
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    },

    transpile: ['vuetify-datetime-picker'],
    // extend(config) {
    //   config.module?.rules.push({
    //     test: /\.[cm]?js$/,
    //     use: {
    //       loader: 'babel-loader',
    //       options: {
    //         presets: ['@babel/preset-env'],
    //       },
    //     },
    //   })
    // },
  },
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: { url: 'auth/login', method: 'post', propertyName: 'access_token' },
          logout: false,
          user: { url: 'auth/profile', method: 'get', propertyName: 'data.attributes' }
        },
        // tokenRequired: true,
        tokenType: 'Bearer',
      },
    },
    redirect: {
      home: false,
      callback: false,
      logout: false
    },
    // cookie: {
    //   options:{
    //     httpOnly: true
    //   }
    // }
  },
  i18n: {
    locales: ['en', 'es'],
    defaultLocale: 'en',
    vueI18n: {
      fallbackLocale: 'es',
      messages: {
        en: {
          welcome: 'Welcome',
          category: 'Category',
          price: 'Price',
          home: 'Home',
          noLogin: 'Log in to access',
          add:'Add'
        },
        es: {
          welcome: 'Bienvenido',
          category: 'Categoria',
          price: 'Precio',
          home: 'Principal',
          noLogin: 'Iniciar sesión para acceder a',
          add:'Agregar'
        }
      }
    }
  }
}
