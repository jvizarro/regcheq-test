export interface Snackbar{
  showing?: boolean
  color?: string
  text: string
}
