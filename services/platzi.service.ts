import { $axios } from "~/utils/api"
import type { IProduct } from "~/interfaces/IProduct"

export class PlatziService {
  public async getProducts(){
    const response = await $axios.get<IProduct[]>('products')
    return response.data
  }
}
