import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators'
import { PlatziService } from '~/services/platzi.service'
import type { IProduct } from '~/interfaces/IProduct'
import {store} from '@/store'

const platziService = new PlatziService()

@Module({
  name:'modules/product',
  stateFactory: true,
  namespaced: true
})
export default class ProductModule extends VuexModule {
  products = <IProduct[]>[]

  @Mutation
  SET_PRODUCTS(newProducts: IProduct[]){
    this.products = newProducts
  }
  @Action({ rawError: true})
  async getProducts(){
    const data = await platziService.getProducts()
    console.log(data)
    return data
    // this.SET_PRODUCTS(data)
  }
}
