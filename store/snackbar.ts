// export const state = () => ({
//   snackbars: []
// })

// export const mutations = {
//   SET_SNACKBAR(state, snackbar) {
//     state.snackbars = state.snackbars.concat(snackbar);
//   },
// }

// export const actions = {
//   setSnackbar({commit}, snackbar) {
//     snackbar.showing = true;
//     snackbar.color = snackbar.color || 'success';
//     commit('SET_SNACKBAR', snackbar);
//   },
// }
import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators'
import type { Snackbar } from '@/interfaces/ISnackbar'

@Module({
  name:'modules/snackbar',
  stateFactory: true,
  namespaced: true
})
export default class SnackbarModule extends VuexModule {
  snackbars = <Snackbar[]>[]

  @Mutation
  SET_SNACKBAR(snackbar: Snackbar) {
    this.snackbars = this.snackbars.concat(snackbar);
  }

  @Action
  setSnackbar(snackbar: Snackbar) {
    snackbar.showing = true;
    snackbar.color = snackbar.color || 'success';
    this.SET_SNACKBAR(snackbar)
  }
}
