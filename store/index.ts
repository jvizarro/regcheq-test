// import { getAccessorType } from 'typed-vuex'
// import Vuex from 'vuex'

// // Import all your submodules
// import userModule from '~/store/user'

// // Keep your existing vanilla Vuex code for state, getters, mutations, actions, plugins, etc.
// // ...

// // This compiles to nothing and only serves to return the correct type of the accessor
// // export const accessorType = getAccessorType({
// //   // state,
// //   // getters,
// //   // mutations,
// //   // actions,
// //   state: {},
// //   modules: {
// //     // The key (submodule) needs to match the Nuxt namespace (e.g. ~/store/submodule.ts)
// //     userModule,
// //   },
// // })

// export function createStore(){
//   return new Vuex.Store({
//     modules: {
//       userModule
//     }
//   })
// }


import Vuex, { Store } from 'vuex'
// import ProductModule from './product'
import { initialiseStores } from '~/utils/store-accessor'

const initializer = (store: Store<any>) => initialiseStores(store)

export const plugins = [initializer]
export * from '~/utils/store-accessor'

export const store = new Vuex.Store<any>({});

// export function createStore() {
//   return new Vuex.Store({
//     modules: {
//       product: ProductModule
//     }
//   })
// }
