import { Store } from 'vuex'
import { getModule } from 'vuex-module-decorators'
import UserModule from '~/store/user'
import SnackbarModule from '~/store/snackbar'
import ProductModule from '~/store/modules/product'

let userStore: UserModule
let snackbarStore: SnackbarModule
let productStore: ProductModule

function initialiseStores(store: Store<any>): void {
  userStore = getModule(UserModule, store)
  snackbarStore = getModule(SnackbarModule, store)
  productStore = getModule(ProductModule, store)
}

export {
  initialiseStores,
  userStore,
  snackbarStore,
  productStore
}
