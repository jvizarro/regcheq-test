import type { Middleware } from "@nuxt/types"
const middleware: Middleware = ({$auth, route, redirect, store})  => {
  if(!$auth.getToken('local')) {
    const REDIRECT_URL = '/auth?redirect=' + route.path
    // store.dispatch('snackbar/setSnackbar', {color: 'error', text: "You must be logged in to view that page."})
    redirect(REDIRECT_URL)
  }
}
export default middleware
